// NUMBER 1
const namaSiswa = ['Tata', 'Koko', 'Lala', 'Sisi', 'Tono'];


// NUMBER 2
const biodata = {
  name: "Tono Kusumo",
  age: 20,
  gender: "Male",
  address: {
    city: "Jakarta",
    country: "Indonesia",
  }
};


// NUMBER 3
const [one, two, three] = namaSiswa;
console.log(one, two, three);

const {name, age, gender, address} = biodata;
console.log(name , age, gender);
console.log(address);
const {city, country} = address;


// NUMBER 4
console.log(`Hi! I'm live in ${address.city}`);


// NUMBER 5
let isEating = true;

const eat = (isEating === true) ? "I've eaten already" : "I'm very hungry";
console.log(eat);


// NUMBER 6
let studentScores = [
  {name: "Tata", score: 8},
  {name: "Koko", score: 6},
  {name: "Lala", score: 9},
  {name: "Sisi", score: 8},
  {name: "Tono", score: 7}
];


// NUMBER 7
const point = studentScores.map(studentScore => studentScore.score * 10);
console.log(point);


// NUMBER 8
const pass = studentScores.filter(studentScore => studentScore.score >= 8);
console.log(pass);


// NUMBER 9
const failed = studentScores.find(studentScore => studentScore.score < 8);
console.log(failed);